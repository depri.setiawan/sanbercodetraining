/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { View, ScrollView} from 'react-native';
import firebase from '@react-native-firebase/app';



import Tugas1 from './src/screen/Tugas1/Tugas1';
import Tugas2 from './src/screen/Tugas2';
import Tugas3 from './src/screen/Tugas3/Todolist';
import Tugas4 from './src/screen/Tugas4'
import AppNavigation from './src/navigations/routes'
import Login from './src/screen/Login'

var firebaseConfig = {
  apiKey: "AIzaSyCcg44WWml0vHzI9nmOMYQid4wOLwwl8KU",
  authDomain: "depritrainingapp.firebaseapp.com",
  projectId: "depritrainingapp",
  storageBucket: "depritrainingapp.appspot.com",
  messagingSenderId: "651341347353",
  appId: "1:651341347353:web:45f5a68e4fcdc13233c888",
  measurementId: "G-86K1671YZ5",
  databaseURL:"https://depritrainingapp-default-rtdb.firebaseio.com/"
};

// Initialize Firebase
if(!firebase.app.length){
firebase.initializeApp(firebaseConfig);
//firebase.analytics();

}

const App = () => {
  return (
    <View style={{flex:1}}>
     
        <AppNavigation/>
       
    </View>
   
  );
}


export default App;
