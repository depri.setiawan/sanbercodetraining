export const colors = {
    white: '#ffffff',
    black: '#000',
    blue: "#3885fb",
    green:"#27ae60",
    lightGrey:'#dddddd',
    grey:'#555555'
  }