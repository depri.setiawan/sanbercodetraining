import React,{useEffect, useState,useRef} from 'react';
import {View,TextInput,Text,TouchableOpacity,StyleSheet,Alert,Image} from 'react-native'
import {Button} from '../../component/Button'
import {styles} from './style'
import api from '../../api'
import Axios from 'axios'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import PasswordInputText from 'react-native-hide-show-password-input'
import Icon from 'react-native-vector-icons/Feather';

const SetupPassword=({navigation,route})=>{
    let input=useRef(null)
    const[email,setEmail]=useState(route.params.email);
    const[password,setPassword]=useState('');
    const[password_confirmation,setPasswordConfirmation]=useState('');
    const[hidePassword,setHidePassword]=useState(true);
    const[hidePasswordConfirm,setHidePasswordConfir]=useState(true);

    const onChangePasswordPress=()=>{
       
        let body={
            email:email,
            password:password,
            password_confirmation:password_confirmation
        }
        if(password.length<6 || password_confirmation<6){
            Alert.alert('Perhatian','Password tidak boleh kurang dari 6 karakter');

        }else if(password!==password_confirmation){
            Alert.alert('Perhatian','Password tidak sama');
        }else{
             Axios.post(`${api.api}/auth/update-password`,body,{
                timeout:20000
            },JSON.stringify(body))
            .then((res)=>{
                console.log('Daftar->Res',res.data);
                if(res.data.response_code=='00'){
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Login' }],
                    });
                }else if(res.data.response_code!='00'){
                    Alert.alert("Perhatian",res.data.response_message);
                }else{
                    Alert.alert("Perhatian","Gagal Menyimpam Password");
                }
                
            })
            .catch((e)=>{
                Alert.alert("Perhatian","Gagal Menyimpam Password");
                console.log("Change Password",e);
            })
        }   
    }
    
   

    return(
       
            <View style={styles.container}>
                <Text style={styles.titleLogin}>Tentukan kata sandi baru untuk keamanan akun kamu</Text>
                
               {/*  <PasswordInputText
                    getRef={input=>input}
                    value={password}
                    onChangeText={(password) => setPassword(password )}
                    style={{width:'80%'}}
                 />
                 <PasswordInputText
                    getRef={input=>input}
                    value={password_confirmation}
                    onChangeText={(password_confirmation) => setPasswordConfirmation(password_confirmation)}
                    style={{width:'80%'}}
                 /> */}
        <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
        <TextInput underlineColorAndroid = "transparent" 
            secureTextEntry = { hidePassword} style = { styles.inputText }
            value={password}
            onChangeText={(password)=>setPassword(password)}/>

        <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = {()=>setHidePassword(!hidePassword) }>
                <Icon name={ ( !hidePassword ) ? "eye":"eye-off"} size={30}/>
        </TouchableOpacity>
        </View>

        <View style={{flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
        <TextInput underlineColorAndroid = "transparent" 
            secureTextEntry = { hidePasswordConfirm} style = { styles.inputText }
            value={password_confirmation}
            onChangeText={(password_confirmation)=>setPasswordConfirmation(password_confirmation)}/>

        <TouchableOpacity activeOpacity = { 0.8 } style = { styles.visibilityBtn } onPress = {()=>setHidePasswordConfir(!hidePasswordConfirm) }>
                <Icon name={ ( !hidePasswordConfirm ) ? "eye":"eye-off"} size={30}/>
        </TouchableOpacity>
        </View>


                <Button onPress={()=>onChangePasswordPress()}>
                       <View style={styles.btnSavePassword}>
                                <Text style={styles.btnTextSavePassword}>SIMPAN</Text>
                        </View>
                </Button>
                
               
            </View>
           
    
    
    )

}

export default SetupPassword