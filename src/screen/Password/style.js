import {StyleSheet} from 'react-native'
import {colors} from '../../style/colors'

export const styles=StyleSheet.create({
    container:{
        flex:1,
        marginTop:10,
        padding:20,
        justifyContent:'flex-start',
        alignItems:'center',


    },
    titleLogin:{
        fontSize:15,
        fontWeight:'bold',
        textAlign:'center'
    },
    titleDesc:{
        fontSize:15,
        textAlign:'center'
    },

    inputText:{
        marginTop:30,
        textAlign:'left',
        width:'70%',
        borderBottomWidth:1,
        borderColor:'#bbbbbb',
    },
    
  
    btnSavePassword:{
        marginTop:30,
        padding:10,
        width:300,
        backgroundColor:colors.blue,
        alignItems:'center',
        borderRadius:10
    },

    
    btnTextSavePassword:{
        fontSize:14,
        fontWeight:'bold',
        color:colors.white
    },

   
    footerContainer:{
        justifyContent:'center',
        alignItems:'center'
    },

    footer1:{
        fontSize:14,
      
    },
    footer2:{
        fontSize:14,
        fontWeight:'bold',
        color:colors.blue
    },
    borderStyleBase: {
        width: 30,
        height: 45
      },
    
      borderStyleHighLighted: {
        borderColor: "#3498db",
        color:colors.black,
        fontSize:25
      },
    
      underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
        color:colors.black,
        fontSize:25
      },
    
      underlineStyleHighLighted: {
        borderColor: "#2980b9",
        color:colors.black,
        fontSize:25
      },
   


});