import React,{useState,useEffect} from 'react'
import { View, Text, Image, FlatList, ScrollView, TouchableOpacity, StyleSheet } from 'react-native'
import {colors} from '../../style/colors'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import MapboxGL from '@react-native-mapbox-gl/maps'


MapboxGL.setAccessToken('pk.eyJ1IjoiZGVwcmlzZXRpYXdhbiIsImEiOiJjand5ZDg1MXUxM2R1NDlsZm42c29xYWtlIn0.dUT5ImopsbKCkzn40s62XQ')

const Map=({navigation})=>{
    useEffect(()=>{
            const getLocation=async()=>{
                try{
                    const permission=await MapboxGL.requestAndroidLocationPermissions()
                }catch(error){
                    console.log(error)
                }
            }
            getLocation()

    },[])

    return(
        <View style={styles.container}>
            <View style={{height:'50%'}}>
            <MapboxGL.MapView
                styleURL={MapboxGL.StyleURL.Street}
                zoomLevel={16}
                centerCoordinate={[110.365231, -7.795766]}
                style={{height:'100%'}}>
                <MapboxGL.UserLocation visible={true}/>
                <MapboxGL.Camera
                    zoomLevel={5}
                    centerCoordinate={[110.365231, -7.795766]}
                    animationMode={'flyTo'}
                    animationDuration={0}
                    >
                </MapboxGL.Camera>

                <MapboxGL.PointAnnotation
                    key="pointAnnotation1"
                    id="pointAnnotation1"
                    coordinate={[110.365231, -7.795766]}>
                 <MapboxGL.Callout title="Longitude:110.365231 Latitude:-7.795766"
                contentStyle={{width:150}}/>
                </MapboxGL.PointAnnotation> 

                <MapboxGL.PointAnnotation
                    key="pointAnnotation2"
                    id="pointAnnotation2"
                    coordinate={[107.580110, -6.890066]}>
                 <MapboxGL.Callout title="Longitude:107.580110 Latitude:-6.890066"
                contentStyle={{width:150}}/>
                </MapboxGL.PointAnnotation>
                
                <MapboxGL.PointAnnotation
                    key="pointAnnotation3"
                    id="pointAnnotation3"
                    coordinate={[106.819449, -6.218465]}>
                <MapboxGL.Callout title="Longitude:106.819449 Latitude:-6.218465"
                contentStyle={{width:150}}/>
    
                </MapboxGL.PointAnnotation>

                
                </MapboxGL.MapView>
            </View>
            
            <View style={{height:'50%',justifyContent:'flex-start'}}>
                <View style={styles.itemMap}>
                    <MaterialCommunityIcons name="home-city-outline" size={20}/>
                    <Text style={styles.itemText}>
                            Jakarta, Bandung, Jogya
                    </Text>
                </View>
                <View style={styles.itemMap}>
                    <MaterialCommunityIcons name="at" size={20}/>
                    <Text style={styles.itemText}>
                            customer_service@crowdfunding.com
                    </Text>
                </View>
                <View style={styles.itemMap}>
                    <MaterialCommunityIcons name="phone-outline" size={20}/>
                    <Text style={styles.itemText}>
                            (021) 777-888
                    </Text>
                </View>
            </View>
        </View>
    )
}

export default Map

const styles=StyleSheet.create({
    container:{
       flex:1,
    },
    itemMap:{
        flexDirection:'row',
        borderColor:'#dddddd',
        borderBottomWidth:1,
        height:60,
        width:'100%',
        padding:20
        
    },
    itemText:{
        fontSize:14,
        marginLeft:10,
        fontWeight:'bold'
    }
    
})