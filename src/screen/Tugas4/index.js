import React,{useState,createContext} from 'react'
import TodoList from './TodoList';

export const RootContext=createContext();

const Context=()=>{
    const[input,setInput]=useState('');
    const[todos,setTodoList]=useState([]);
    
    handleChangeInput=(value)=>{
        setInput(value)
    }

    addTodo=()=>{
        let day=new Date().getDate();
        let month=(new Date().getMonth())+1;
        let year=new Date().getFullYear();
        month=(month<10?'0'+month:month);
        let today=`${day}/${month}/${year}`
        if(input.length>0){
            setTodoList([...todos,{title:input,date:today}]);
            setInput('');
        }
    }

    hapusTodo=(id)=>{
        setTodoList(todos.filter((todo,index)=>index!=id));
    }

    return(
        <RootContext.Provider value={{
            input,
            todos,
            handleChangeInput,
            addTodo,
            hapusTodo
        }}>

               <TodoList/> 
        </RootContext.Provider>
    )
}

export default Context;

