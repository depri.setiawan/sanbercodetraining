import React, {useContext} from 'react';
import {Button,StatusBar,StyleSheet,Text,TextInput,TouchableOpacity,View,FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {RootContext} from '.';

const TodoList = () => {
    const state=useContext(RootContext);
    console.log("TodoList->state",state);

   const renderItem=({item,index})=>{
     return(
          <View style={styles.item}>
              <View>
                  <Text>{item.date}</Text>
                  <Text>{item.title}</Text>
              </View>
              <TouchableOpacity onPress={()=>state.hapusTodo(index)}>
              <Icon name="trash-outline" size={30} color="#000000" />
              </TouchableOpacity>
          </View>
     );
   }

   return (
    <View style={styles.wrapper}>
      <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
      <Text>Masukkan Todolist</Text>
      <View style={styles.inputWrapper}>
        <TextInput style={styles.textInput}
        onChangeText={(value)=>state.handleChangeInput(value)}
        value={state.input}
        
         
        />
        <TouchableOpacity activeOpacity = { 0.8 } style={styles.button} 
        onPress={()=>state.addTodo()}>
            <Text style={{color:'#ffffff',fontWeight:'bold'}}>+</Text>
        </TouchableOpacity>
       
        
      </View>
      <View>
        <FlatList style={styles.listContainer}
          data={state.todos}
          renderItem={renderItem}
          keyExtractor={(item, index) => index.toString()}
         />
      </View>
     
    </View>
  );
};

export default TodoList;

const styles=StyleSheet.create({
   wrapper:{
      padding:15
   },
   inputWrapper:{
     flexDirection:'row',
     height:50,
     marginBottom:10
     
   },
   textInput:{
     width:'85%',
     borderColor:'gray',
     borderWidth:1
   },
   button: {
    alignItems: "center",
    padding: 10,
    width:'15%',
    justifyContent:'center',
    backgroundColor:'#3498db'
  },
  item:{
    flexDirection:'row',
    padding:20,
    borderWidth:1,
    borderColor:'grey',
    marginBottom:10,
    alignItems:'center',
    justifyContent:'space-between'
  }

});