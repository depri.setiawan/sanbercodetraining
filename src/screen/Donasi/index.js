import React,{useState,useEffect} from 'react'
import { View, Text, Image, FlatList, ScrollView, TouchableOpacity, StyleSheet } from 'react-native'
import {colors} from '../../style/colors'
import api from '../../api'
import Axios from 'axios'
import Asyncstorage from '@react-native-async-storage/async-storage'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {ProgressBar} from 'react-native-paper'

const Donasi=({navigation})=>{
    const [token, setToken] = useState('')
    const [donasi,setDonasi]=useState([])
    useEffect(() => {
        const getToken=async()=>{
            try{
                const token=await Asyncstorage.getItem('token')
                if(token!==null){
                    setToken(token)
                    console.log("Donasi->token",token)
                    getDonasi(token)
                }
            }catch(err){
                console.log(err)
            }
        }

        getToken();
        
       
    },[])

    const getDonasi=(token)=>{
        Axios.get(`${api.api}/donasi/daftar-donasi`,{
            timeout:20000,
            headers:{
                'Authorization':'Bearer'+token
            }
        })
        .then((res)=>{
            const data=res.data.data.donasi
            setDonasi(data)
            console.log("getDonasi-res",res.data.data.donasi)
            
        })
        .catch((error)=>{
            console.log("editAccount-error",error)
        })
    }    
    
    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    const renderItem=({item,index})=>{
        return(
                    <View style={styles.containerDonasi}>
                     <View style={styles.containerFoto}>
                         <Image source={(item.photo)==null?require('../../asset/images/no_photo.png'):{uri:api.home+item.photo}} 
                            style={{width:200,
                                height:150,
                                borderRadius:10                               
                                }}/>
                    
                     </View>
                     <View style={styles.containerInfo}>
                            <Text style={{fontWeight:'bold',fontSize:16}} >{item.title}</Text>    
                            <Text >{item.user.name}</Text> 
                            <ProgressBar progress={0.2} color={colors.blue} />   
                            <Text >Dana yang dibutuhkan</Text>    
                            <Text style={{fontWeight:'bold'}} >Rp. {formatNumber(item.donation)}</Text>    
                               
                     </View>
                         
                     </View>
                 
        );
      }

    return(
        
            <View style={styles.container}>
                        <FlatList
                        data={donasi}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => index.toString()}
                        />
            </View>
     
    )
}

export default Donasi

const styles=StyleSheet.create({
  container:{
      flex:1,
      justifyContent:'center'
  },
  containerDonasi:{
      flexDirection:'row',
      margin:10,
     
  },
  containerInfo:{
     flex:1,
      padding:10,
      flexDirection:'column',
      justifyContent:'center'
      
  }
  
})