import React, { useEffect, useState } from 'react'
import { View, Text, Image, StatusBar, TouchableOpacity,Alert } from 'react-native'
import styles from './style'
import Icon from 'react-native-vector-icons/Ionicons';
import uri from '../../api'
import {colors} from '../../style/colors'
import Axios from 'axios'
import Asyncstorage from '@react-native-async-storage/async-storage'
import { GoogleSignin } from '@react-native-community/google-signin';
import { useIsFocused } from "@react-navigation/native";

const Account = ({ navigation }) => {

    const [data, setData] = useState({})
    const [userInfo, setUserInfo] = useState(null)
    const [isGoogle, setIsGoogle] = useState(false)
    const isFocused = useIsFocused();
    useEffect(() => {
        if(isFocused){
       
        async function getToken(){
            try{
              const token=await Asyncstorage.getItem("token")
              console.log('GetToken->token',token)
              getProfile(token)
              console.log('GetProfile->profile',data)
            }catch(err){
              console.log(err)
            }
          }
          getToken()
          getCurrentUser();
        }
          //getProfile(token);
       // console.log("Account-userInfo",userInfo)
    }, [isFocused])

    const getCurrentUser =  async () => {
        try {
            const userInfo = await GoogleSignin.signInSilently()
            if(userInfo) {
                setUserInfo(userInfo)
                setIsGoogle(true)
            }
        } catch(error) {
            setIsGoogle(false)   
        }
    }

    const getProfile = (token) => {
        Axios.get(`${uri.api}/profile/get-profile`, {
            headers: {
                'Authorization': 'Bearer' + token,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        })
        .then((res) => {
            const data = res.data.data.profile
            setData(data)
        })
        .catch((err) => {
            console.log("Account -> err", err)  
        })
    }

    const onLogoutPress = async () => {
        try{
            Alert.alert('Keluar','Apakah anda yakin untuk keluar???',
            [
                {text:'No'
                },
                {
                 text:'Yes',
                 onPress:async()=>{
                  if(isGoogle){
                    await GoogleSignin.revokeAccess()
                    await GoogleSignin.signOut()
                  }
                  await Asyncstorage.removeItem("token")
                  navigation.reset({
                    index: 0,
                    routes: [{ name: 'Login' }],
                 });
                 }
      
                }
            ]);
           
          }catch(e){
            console.log(e)
          }
    }

    const editProfile=()=>{
        navigation.navigate('EditAccount',{
          name:data.name,
          email:data.email,
          photo:data.photo
        })
      }
    
      const showMaps=()=>{
        navigation.navigate('Map')
      }

    return (
        <View style={styles.container}>
            <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
            <TouchableOpacity style={styles.userContainer} activeOpacity={0.7} onPress={() => editProfile()} >
                <Image source={isGoogle == false ? data.photo == null ? require('../../asset/images/avatar.png') : { uri : uri.home + data.photo + '?' + new Date(), cache: 'reload', headers: {Pragma: 'no-cache' } } : { uri: userInfo && userInfo.user && userInfo.user.photo } } style={styles.picture} />
                <Text style={styles.name}>{isGoogle == false ? data.name : userInfo && userInfo.user && userInfo.user.name }</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.purseContainer} activeOpacity={0.7}>
                <View style={styles.icon}>
                <Icon name="ios-wallet-outline" size={30} color="#000000" />
                <Text>Saldo</Text>
                </View>
                <Text>Rp. 120.000.000</Text>
            </TouchableOpacity>
            <View style={styles.menu}>
                <TouchableOpacity style={styles.button} activeOpacity={0.7}>
                <Icon name="settings-outline" size={30} color="#000000" />
                <Text>Pengaturan</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} activeOpacity={0.7} onPress={() => showMaps()}>
                <Icon name="help-circle-outline" size={30} color="#000000" />
                  <Text>Bantuan</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} activeOpacity={0.7}>
                <Icon name="newspaper-outline" size={30} color="#000000" />
              <Text>Syarat & Ketentuan</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.logout}>
                <TouchableOpacity style={styles.button} activeOpacity={0.7} onPress={() => onLogoutPress()}>
                <Icon name="ios-exit-outline" size={30} color="#000000" />
               <Text>Keluar</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Account
