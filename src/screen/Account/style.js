import React from 'react'
import { StyleSheet, Dimensions } from 'react-native'
import {colors} from '../../style/colors'

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const style = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    userContainer: {
        height: '13%',
        padding: 20,
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 0.9,
        backgroundColor: colors.white,
        borderBottomColor: colors.lightGrey
    },
    profileContainer: {
        marginTop: 20,
        marginBottom: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    rounded: {
        left: (width / 2) - 25,
        width: 30,
        height: 30,
        position: 'absolute',
        borderRadius: 30 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.grey
    },
    picture: {
        width:50,
        height: 50,
        marginRight: 20,
        borderRadius: 50 / 2,
        alignItems: 'center',
        justifyContent: 'center'
    },
    profile: {
        width:80,
        height: 80,
        marginRight: 20,
        borderRadius: 80 / 2
    },
    name: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    purseContainer: {
        padding: 20,
        height: '10%',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: colors.white
    },
    icon: {
        alignItems: 'center',
        flexDirection: "row"
    },
    image: {
        width: 25,
        height: 25,
        marginRight: 20
    },
    menu: {
        marginTop: 5,
        flexDirection: 'column',
        backgroundColor: colors.white
    },
    button: {
        padding: 20,
        alignItems: 'center',
        flexDirection: 'row',
        borderBottomWidth: 0.9,
        borderBottomColor: colors.lightGrey
    },
    logout: {
        marginTop: 5,
        flexDirection: 'column',
        backgroundColor: colors.white
    },
    detailUser: {
        padding: 20,
        flexDirection: 'column'
    },
    editContainer: {
        flexDirection: 'column',
        justifyContent: 'center'
    },
    editItem: {
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    input: {
        flex: 1,
        marginLeft: -5,
        color: colors.black
    },
    editTitle: {
        fontSize: 14,
        color: colors.grey
    },
    saveBtn: {
        height: 50,
        width: '100%',
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.blue
    },
    btnSaveText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: colors.white
    },
    camFlipContainer: {
        marginTop: 20,
        marginLeft: 20,
        justifyContent: 'flex-start'
    },
    btnFlip: {
        height: 40,
        width: 40,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.white
    },
    round: {
        marginTop: 20,
        borderWidth: 1,
        height: height / 2.7,
        width: width / 2,
        borderRadius: 100,
        borderColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: 'transparent'
    },
    rectangle : {
        marginTop: 70,
        width: width / 2,
        height: height / 6,
        borderWidth: 1,
        borderColor: 'white',
        alignSelf: 'center',
        justifyContent: 'flex-end',
        backgroundColor: 'transparent'
    },
    btnTakeContainer: {
        flex: 1,
        marginBottom: 30,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    btnTake: {
        height: 70,
        width: 70,
        borderRadius: 35,
        alignItems: 'center',
        backgroundColor: '#ffffff',
        justifyContent: 'center'
    },
})

export default style
