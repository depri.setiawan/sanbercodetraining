import React, { useState, useRef, useEffect } from 'react'
import { View, Text, Image, Modal, TextInput, TouchableOpacity, ToastAndroid } from 'react-native'
import styles from './style'
import uri from '../../api'
import Axios from 'axios'
import {colors} from '../../style/colors'
import Icon from 'react-native-vector-icons/Feather'
import { Button } from '../../component/Button'
import { RNCamera } from 'react-native-camera'
import AsyncStorage from '@react-native-async-storage/async-storage'
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons'

const editAccount = ({ navigation, route }) => {

    let input = useRef(null)
    let camera = useRef(null)
    const [editable, setEditable] = useState(false)
    const [token, setToken] = useState('')
    const [name, setName] = useState(route.params.name)
    const [email, setEmail] = useState(route.params.email)
    const [isVisible, setIsVisible] = useState(false)
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)

    const toggleCamera = () => {
        setType(type == 'back' ? 'front' : 'back')
    }

    const takePicture = async () => {
        const options = { quality: 0.5, base64: true };
        if (camera) {
            const data = await camera.current.takePictureAsync(options);
            setPhoto(data)
            setIsVisible(false)
        }
    }

    useEffect(() => {
        const getToken = async() => {
            try {
                const token = await AsyncStorage.getItem('token')
                if(token !== null) {
                    setToken(token)
                }
            } catch(err) {
                console.log(err)
            }
        }
        getToken()
    }, [])

    const editData = () => {
        setEditable(!editable)
    }

    const onSavePress = () => {
        const formData = new FormData()
        formData.append('name', name)
        formData.append('photo', {
            uri: photo.uri,
            name: 'photo.jpg',
            type: 'image/jpg'
        })
        Axios.post(`${uri.api}/profile/update-profile`, formData, {
            timeout: 20000,
            headers: {
                'Authorization': 'Bearer' + token,
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        })
        .then((res) => {
            navigation.goBack()
            if(res) {
                ToastAndroid.showWithGravity(
                    'Profile berhasil diupdate',
                    ToastAndroid.LONG,
                    ToastAndroid.BOTTOM,
                    0,
                    50
                )
            }
        })
        .catch((err) => {
            console.log("editAccount -> err", err)    
        })
    }

    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1 }}
                        type={type}
                        ref={camera}
                    >
                        <View style={styles.camFlipContainer}>
                            <TouchableOpacity style={styles.btnFlip} onPress={() => toggleCamera()}>
                                <MaterialCommunity name="rotate-3d-variant" size={15} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.round} />
                        <View style={styles.rectangle} />
                        <View style={styles.btnTakeContainer}>
                            <TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
                                <Icon name="camera" size={30} />
                            </TouchableOpacity>
                        </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return (
        <View style={[styles.container, { backgroundColor: colors.white }]}>
            <View style={styles.profileContainer}>
                <Image source={route.params.photo !== null && photo == null ? { uri : uri.home + route.params.photo + '?' + new Date(), cache: 'reload', headers: {Pragma: 'no-cache' } } : route.params.photo === null && photo !== null ? {uri: photo.uri} : route.params.photo !== null && photo !== null ? {uri: photo.uri} : require('../../asset/images/avatar.png')} style={styles.profile} />
                <TouchableOpacity style={styles.rounded} activeOpacity={0.7} onPress={() => setIsVisible(true)}>
                    <Icon name="camera" size={15} color={colors.white} />
                </TouchableOpacity>
            </View>
            <View style={styles.detailUser}>
                <View style={styles.editContainer}>
                    <View>
                        <Text style={styles.editTitle}>Nama Lengkap</Text>
                        <View style={styles.editItem}>
                            <TextInput
                                ref={input}
                                value={name}
                                editable={editable}
                                style={styles.input}
                                onChangeText={(value) => setName(value)}
                            />
                            <Icon name="edit-2" size={20} color={colors.grey} onPress={() => editData()} />
                        </View>
                    </View>
                    <View>
                        <Text style={styles.editTitle}>Email</Text>
                        <View style={styles.editItem}>
                            <TextInput
                                value={email}
                                editable={false}
                                style={styles.input}
                                onChangeText={(value) => setEmail(value)}
                            />
                            {/* <Icon name="edit-2" size={20} color={colors.grey} /> */}
                        </View>
                    </View>
                </View>
                <View style={{ marginTop: 30 }}>
                    <Button activeOpacity={0.7} onPress={() => onSavePress()}>
                            <View style={styles.saveBtn}>
                            <Text style={styles.btnSaveText}>Simpan</Text>
                        </View>
                    </Button>
                </View>
            </View>
            {renderCamera()}
        </View>
    )
}

export default editAccount
