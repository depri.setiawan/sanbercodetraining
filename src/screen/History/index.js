import React,{useState,useEffect} from 'react'
import { View, Text, Image, FlatList, ScrollView, TouchableOpacity, StyleSheet } from 'react-native'
import {colors} from '../../style/colors'
import api from '../../api'
import Axios from 'axios'
import Asyncstorage from '@react-native-async-storage/async-storage'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import {ProgressBar} from 'react-native-paper'

const History=({navigation})=>{
    const [token, setToken] = useState('')
    const [riwayat,setRiwayat]=useState([])
    useEffect(() => {
        const getToken=async()=>{
            try{
                const token=await Asyncstorage.getItem('token')
                if(token!==null){
                    setToken(token)
                    console.log("Donasi->token",token)
                    getRiwayat(token)
                }
            }catch(err){
                console.log(err)
            }
        }
        getToken();
       
       
    },[])

    const getRiwayat=(token)=>{
        Axios.get(`${api.api}/donasi/riwayat-transaksi`,{
            timeout:20000,
            headers:{
                'Authorization':'Bearer'+token
            }
        })
        .then((res)=>{
            const data=res.data.data.riwayat_transaksi
            setRiwayat(data)
            console.log("Riwayat-res",data)
            
        })
        .catch((error)=>{
            console.log("Riwayat-error",error)
        })
    }    
    
    function formatNumber(num) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
    }

    const renderItem=({item,index})=>{
        return(
                    <View style={styles.containerDonasi}>
                     <View style={styles.containerInfo}>
                            <Text style={{fontWeight:'bold',fontSize:16}} >Total Rp. {formatNumber(item.amount)}</Text>    
                            <Text >Donation-ID : {item.id}</Text> 
                            <Text >Tanggal Transaksi : {item.updated_at} </Text>    
                               
                     </View>
                         
                     </View>
                 
        );
      }

    return(
        
            <View style={styles.container}>
                        <FlatList
                        data={riwayat}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => index.toString()}
                        />
            </View>
     
    )
}

export default History

const styles=StyleSheet.create({
  container:{
      flex:1,
      justifyContent:'center'
  },
  containerInfo:{
     flex:1,
      padding:20,
      flexDirection:'column',
      justifyContent:'center',
      borderBottomColor:'#dddddd',
      borderBottomWidth:5,
      marginBottom:5

      
  }
  
})