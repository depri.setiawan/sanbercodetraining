import React,{useEffect, useState} from 'react';
import {View,TextInput,Text,TouchableOpacity,StyleSheet,Alert,Image} from 'react-native'
import {Button} from '../../component/Button'
import {styles} from './style'
import api from '../../api'
import Axios from 'axios'
import Asyncstorage from '@react-native-async-storage/async-storage'
import facebook from '../../asset/images/facebook.png'
import google from '../../asset/images/google.png'
import { NavigationHelpersContext } from '@react-navigation/native';

const Register=({navigation})=>{
    const[email,setEmail]=useState('');
    const[name,setName]=useState('');

    const onRegisterPress=()=>{
        let body={
            email:email,
            name:name
        }

        if(email.length>0 && name.length>0){

       Axios.post(`${api.api}/auth/register`,body,{
            timeout:20000
        },JSON.stringify(body))
        .then((res)=>{
            console.log('Daftar->Res',res.data.response_code);
            if(res.data.response_code=='00'){
                navigation.navigate('OTPHandle',{
                    email:email
                })
            }else{
                Alert.alert("Perhatian","Terjadi Kesalahan saat proses pendaftaran");
            }
            setEmail('');
            setName('');
      
            
        })
        .catch((e)=>{
            Alert.alert("Perhatian","Terjadi Kesalahan saat proses pendaftaran");
            console.log('Register->Error',e)
             
        })
        }else{
            Alert.alert("Perhatian","Email/Nama Lengkap tidak boleh kosong");
        }
    }

    return(
       
            <View style={styles.container}>
                <Text style={styles.titleLogin}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</Text>
                <TextInput 
                style={styles.inputEmail}
                placeholder='Nomor Ponsel atau Email'
                value={email}
                onChangeText={(email)=>setEmail(email)}
                />
                <TextInput 
                style={styles.inputEmail}
                placeholder='Nama Lengkap'
                value={name}
                onChangeText={(name)=>setName(name)}
                />
                <Button onPress={()=>onRegisterPress()}>
                       <View style={styles.btnDaftar}>
                                <Text style={styles.btnTextDaftar}>DAFTAR</Text>
                        </View>
                </Button>
                <View style={styles.footerContainer}>
                    <Text style={styles.footer1}>Sudah punya akun ? </Text> 
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Text style={styles.footer2}>Masuk</Text>
                    </TouchableOpacity>
                </View>
    
                <View style={{flex:1,justifyContent:'flex-end'}}>
                    <View style={{margin:5,justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontWeight:'bold',fontSize:16}}>Atau Lebih Cepat Dengan</Text>
                    </View>   
                    <View style={{flexDirection:'row',justifyContent:'center',padding:5}}>
                        <Button>
                            <View style={styles.btnFacebook}>
                                <Image source={facebook} style={{width:30,height:30,marginRight:10}}/>
                                <Text style={styles.btnTextFacebook}>Facebook</Text>
                            </View>
                        </Button>
                        
                        <Button>
                            <View style={styles.btnFacebook}>
                                <Image source={google} style={{width:30,height:30,marginRight:5}}/>
                                <Text style={styles.btnTextFacebook}>Sign In</Text>
                            </View>
                        </Button>
    
                       
    
                        
                    </View>
                    
                 </View>
           
            </View>
           
    
    
    )

}

export default Register