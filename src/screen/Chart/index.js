import React,{useState,useEffect} from 'react'
import { View, Text, Image, FlatList, ScrollView, TouchableOpacity, StyleSheet, processColor,Dimensions } from 'react-native'
import {colors} from '../../style/colors'
import {BarChart} from 'react-native-charts-wrapper' 

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const Chart=({navigation})=>{
    const[legend,setLegend]=useState({
        enabled:false,
        textSize:14,
        form:'SQUARE',
        formSize:14,
        xEntrySpace:10,
        yEntrySpace:5,
        formToTextSpace:5,
        wordWrapEnabled:true,
        maxSizePercent:0.5
    })

    const[chart,setChart]=useState({
        data:{
            dataSets:[{
                values:[{y:100},{y:60},{y:90},{y:45},{y:67},{y:32},{y:150},{y:70},{y:40},{y:89}],
                label:'',
                config:{
                    colors:[processColor(colors.blue)],
                    stackLabels:[],
                    drawFilled:false,
                    drawValues:false
                }
            }]
        }
    })

    const[xAxis,setXAxis]=useState({
        valueFormatter:['Jan','Feb','Mar','Apr','Mei','Jun','Jul','Ags','Sep','Okt','Nop','Des'],
        position:'BOTTOM',
        drawAxisLine:true,
        drawGridLines:false,
        axisMinimum:-0.5,
        granularityEnabled:true,
        granularity:1,
        spaceBetweenLabels:0,
        labelRotationAngle:-45.0,
        limitLines:[{limit:115,lineColor:processColor('red'),lineWidth:1}]
    })

    const[yAxis,setYAxis]=useState({
        left:{
            axisMinimum:0,
            labelCountForce:true,
            granularity:5,
            granularityEnabled:true,
            drawGridLines:false

        },
        right:{
            axisMinimum:0,
            labelCountForce:true,
            granularity:5,
            granularityEnabled:true,
            enabled:false
        }

    })

    return(
        
           <View styles={styles.container}>
               <BarChart 
                style={{ height:'100%'}}
                data={chart.data}
                yAxis={yAxis}
                xAxis={xAxis}
                chartDescription={{text:''}}
                legend={legend}
                pinchZoom={false}
                doubleTapToZoomEnabled={false}
                marker={{
                    enabled:true,
                    markerColor:'#555555',
                    textColor:'white',
                    textSize:14

                }}
                />
           </View>
     
    )
}

export default Chart

const styles=StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'center',
        alignItems:'center',
        width:width,
        height:height
        

    }
})
