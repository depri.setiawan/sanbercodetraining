import {StyleSheet} from 'react-native'
import {colors} from '../../style/colors'

export const styles=StyleSheet.create({
    container:{
        flex:1,
        marginTop:10,
        padding:20,
        justifyContent:'flex-start',
        alignItems:'center',
        overflow:'hidden'

    },
    titleLogin:{
        fontSize:15,
        fontWeight:'bold',
        textAlign:'center'
    },
    inputEmail:{
        marginTop:30,
        textAlign:'left',
        width:'100%',
        borderBottomWidth:1,
        borderColor:'#bbbbbb',
    },
    
    inputPassword:{
        marginTop:30,
        textAlign:'left',
        width:'100%',
        borderBottomWidth:1,
        borderColor:'#bbbbbb',
    },
    btnLogin:{
        marginTop:30,
        padding:10,
        width:300,
        backgroundColor:colors.blue,
        alignItems:'center',
        borderRadius:10
    },
    btnFinger:{
        flexDirection:'row',
        padding:8,
        width:150,
        backgroundColor:colors.white,
        alignItems:'center',
        borderColor:'#dddddd',
        borderWidth:1,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.1,
        shadowRadius: 1,
        elevation: 1,

    },
    btnTextLogin:{
        fontSize:14,
        fontWeight:'bold',
        color:colors.white
    },
    btnTextFinger:{
        fontSize:14,
        fontWeight:'bold',
        color:colors.black
    },
    footerContainer:{
        flexDirection:'row'
    },
    footer1:{
        fontSize:14,
        fontWeight:'bold'
    },
    footer2:{
        fontSize:14,
        fontWeight:'bold',
        color:colors.blue
    }
   


});