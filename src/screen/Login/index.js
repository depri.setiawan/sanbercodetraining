import React,{useEffect, useState} from 'react';
import {View,TextInput,Text,TouchableOpacity,StyleSheet,Alert,Image} from 'react-native'
import {Button} from '../../component/Button'
import {styles} from './style'
import api from '../../api'
import auth from '@react-native-firebase/auth';
import Axios from 'axios'
import Asyncstorage from '@react-native-async-storage/async-storage'
import {GoogleSignin,statusCodes,GoogleSigninButton} from '@react-native-community/google-signin'
import TouchID from 'react-native-touch-id';
import { colors } from '../../style/colors';
import fingerprint from '../../asset/images/fingerprint.png'
import google from '../../asset/images/google.png'

const config={
    title:'Authentication Required',
    imageColor:colors.blue,
    imageErrorCOlor:'#ff0000',
    sensorDescription:'Touch Sensor',
    sensorErrorDescription:'Failed',
    cancelText:'Cancel'
}

const Login=({navigation})=>{
    const[email,setEmail]=useState('');
    const[password,setPassword]=useState('');

    const saveToken=async(item)=>{
        try{
            if(item!==null){
                await Asyncstorage.setItem("token",item.data.token)
            }
        }catch(err){
            console.log(err)
        }
    }

    useEffect(()=>{
         configureGoogleSignIn()
    },[])

    const configureGoogleSignIn=()=>{
        GoogleSignin.configure({
            offlineAccess:false,
            webClientId:'651341347353-1ol4ea1rc1tjiq80k9ssi6oq1pgbvfgj.apps.googleusercontent.com'
        })
    }

    const signInWithGoogle=async()=>{
        try{
            const {idToken}=await GoogleSignin.signIn()
            console.log("SiginWithGoogle->idToken",idToken)
            const credential=auth.GoogleAuthProvider.credential(idToken)
            auth().signInWithCredential(credential)
            navigation.reset({
                index: 0,
                routes: [{ name: 'Account' }],
            });
        }catch(e){
            console.log(e);
        }
    }

    const signInWithFingerPrint=()=>{
        TouchID.authenticate('',config)
        .then(success=>{
            navigation.reset({
                index: 0,
                routes: [{ name: 'Account' }],
            });
        }).
        catch(error=>{
            alert('Authentication Failed')
        })

    }

    const onLoginPress=()=>{
        let body={
            email:email,
            password:password
        }

        if(email.length>0 && password.length>0){
            
        Axios.post(`${api.api}/auth/login`,body,{
            timeout:20000
                },JSON.stringify(body))
                .then((res)=>{
                const data=res.data
                    saveToken(data)
                    //navigation.navigate('Account')
                    navigation.reset({
                        index: 0,
                        routes: [{ name: 'Dashboard' }],
                    });
                    console.log('Login->Res',res.data);
                    setEmail('');
                    setPassword('');
                })
                .catch((e)=>{
                    Alert.alert(
                        'Perhatian',
                        'Username/password salah'
                    )
                    
                })
        }else{

        Alert.alert('Perhatian','Email/Password tidak boleh kosong')

        }
    }

    const daftarAccount=()=>{
        navigation.navigate('Daftar')
        
    }


    return(
        
        <View style={styles.container}>
            <Text style={styles.titleLogin}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore</Text>
            <TextInput 
            style={styles.inputEmail}
            placeholder='Nomor Ponsel atau Email'
            value={email}
            onChangeText={(email)=>setEmail(email)}
            />
            <TextInput 
            style={styles.inputPassword}
            secureTextEntry
            placeholder='Password'
            value={password}
            onChangeText={(password)=>setPassword(password)}
            />
            <Button onPress={()=>onLoginPress()}>
                   <View style={styles.btnLogin}>
                            <Text style={styles.btnTextLogin}>LOGIN</Text>
                    </View>
            </Button>
            
            <View style={styles.footerContainer}>
                <Text style={styles.footer1}>Belum Punya Akun ? </Text> 
                <TouchableOpacity onPress={()=>daftarAccount()}>
                <Text style={styles.footer2}>Daftar</Text>
                </TouchableOpacity>
            </View>

            <View style={{flex:1,justifyContent:'flex-end'}}>
                <View style={{margin:5,justifyContent:'center',alignItems:'center'}}>
                <Text style={{fontWeight:'bold',fontSize:16}}>Atau Lebih Cepat Dengan</Text>
                </View>   
                <View style={{flexDirection:'row',justifyContent:'center',padding:5}}>
                    <Button onPress={()=>signInWithFingerPrint()}>
                        <View style={styles.btnFinger}>
                            <Image source={fingerprint} style={{width:30,height:30,marginRight:10}}/>
                            <Text style={styles.btnTextFinger}>Finger Print</Text>
                        </View>
                    </Button>
                    
                    <Button onPress={()=>signInWithGoogle()}>
                        <View style={styles.btnFinger}>
                            <Image source={google} style={{width:30,height:30,marginRight:5}}/>
                            <Text style={styles.btnTextFinger}>Sign In</Text>
                        </View>
                    </Button>

                   

                    
                </View>
                
             </View>
       
        </View>
       

    );
}



export default Login;