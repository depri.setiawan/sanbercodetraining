import React from 'react';
import {StatusBar, Text, View, StyleSheet} from 'react-native';

const Tugas1 = () => {
  return (
    <View>
      <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
      <View style={styles.container}>
      <Text>Hallo Kelas React Native Lanjutan Sanbercode!</Text>
      </View>
    </View>
  );
};

export default Tugas1;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
