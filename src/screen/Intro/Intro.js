import React from 'react'
import { View, Text, Image, StatusBar, SafeAreaView,StyleSheet } from 'react-native'
import {colors} from '../../style/colors'
import styles from './styles'
import {Button} from '../../component/Button'
import AppIntroSlider from 'react-native-app-intro-slider'

const data = [
    {
        id: 1,
        image: require('../../asset/images/intro_1.png'),
        description: 'Description Text 1'
    },
    {
        id: 2,
        image: require('../../asset/images/intro_2.png'),
        description: 'Description Text 2'
    },
    {
        id: 3,
        image: require('../../asset/images/intro_3.png'),
        description: 'Description Text 3'
    }
]

const Intro = ({ navigation }) => {

    //tampilan onboarding yang ditampilkan dalam renderItem
    const renderItem = ({ item }) => {
        return (
            <View style={styles.listContainer}>
                <View style={styles.listContent}>
                    <Image source={item.image} style={styles.imgList} resizeMethod="auto" resizeMode="contain" />
                </View>
                <Text style={styles.textList}>{item.description}</Text>
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
                <View style={styles.textLogoContainer}>
                    <Text style={styles.textLogo}>CrowdFunding</Text>
                </View>
                <View style={styles.slider}>
                    <AppIntroSlider
                        data={data} 
                        renderItem={renderItem} 
                        renderNextButton={() => null}
                        renderDoneButton={() => null}
                        activeDotStyle={styles.activeDotStyle}
                        keyExtractor={(item) => item.id.toString()}
                    />
                </View>
                <View style={styles.btnContainer}>
                    <Button  onPress={() => navigation.navigate('Login')}>
                        <View style={styles.btnLogin}>
                            <Text style={styles.btnTextLogin}>MASUK</Text>
                        </View>
                    </Button>
                    <Button onPress={() => navigation.navigate('Register')}>
                        <View style={styles.btnRegister}>
                            <Text style={styles.btnTextRegister}>DAFTAR</Text>
                        </View>
                    </Button>
                </View>
            </View>
        </SafeAreaView>
    )
}




export default Intro