import React,{useEffect, useState} from 'react';
import {View,TextInput,Text,TouchableOpacity,StyleSheet,Alert,Image} from 'react-native'
import {Button} from '../../component/Button'
import {styles} from './style'
import api from '../../api'
import Axios from 'axios'
import Asyncstorage from '@react-native-async-storage/async-storage'
import facebook from '../../asset/images/facebook.png'
import google from '../../asset/images/google.png'
import OTPInputView from '@twotalltotems/react-native-otp-input'

const OTPHandle=({navigation,route})=>{
    const[email,setEmail]=useState(route.params.email);
    const[otp,setOTP]=useState('');

    const onVerifikasiPress=()=>{
        let body={
            otp:otp
        }

       Axios.post(`${api.api}/auth/verification`,body,{
            timeout:20000
        },JSON.stringify(body))
        .then((res)=>{
            console.log('Daftar->Res',res);
            if(res.data.response_code=='00'){
                navigation.navigate('SetupPassword',{
                    email:email
                })
            }else if(res.data.response_code!='00'){
                Alert.alert("Perhatian",res.data.response_message);
            }else{
                Alert.alert("Perhatian","Gagal melakukan verifikasi");
            }
            
        })
        .catch((e)=>{
            console.log(e);
        })
    }
    
    const onRegenerateOTPPress=()=>{
        let body={
            email:email
        }

        console.log("Email",email)

       Axios.post(`${api.api}/auth/regenerate-otp`,body,{
            timeout:20000
        },JSON.stringify(body))
        .then((res)=>{
            console.log('Daftar->Res',res.data.response_code);
            if(res.data.response_code=='00'){
                Alert.alert("Perhatian",res.data.response_message);
               
            }else if(res.data.response_code!='00'){
                Alert.alert("Perhatian",res.data.response_message);
                setOTP('');
            }else{
                Alert.alert("Perhatian","Gagal Generate OTP");
                setOTP('');
            }
           
        })
        .catch((e)=>{
            console.log(e);
        })
    }

    return(
       
            <View style={styles.container}>
                <Text style={styles.titleLogin}>Perjalanan Baikmu dimulai dari sini</Text>
                <Text style={styles.titleDesc}>masukkan 6 digit kode yang kami kiriman ke {email}</Text>
                
                <OTPInputView
          style={{width: '80%', height: 200}}
          pinCount={6}
          code={otp} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
          onCodeChanged = {(otp) =>setOTP(otp) }
          autoFocusOnLoad
          codeInputFieldStyle={styles.underlineStyleBase}
          codeInputHighlightStyle={styles.underlineStyleHighLighted}
          onCodeFilled = {(code => {
              console.log(`Code is ${code}, you are good to go!`)
          })}
          // placeholderCharacter={'*'}
          // placeholderTextColor={'red'}
          // selectionColor={"#03DAC6"}
        />
    

                <Button onPress={()=>onVerifikasiPress()}>
                       <View style={styles.btnVerifikasi}>
                                <Text style={styles.btnTextVerifikasi}>VERIFIKASI</Text>
                        </View>
                </Button>
                <View style={styles.footerContainer}>
                    <Text style={styles.footer1}>Belum menerima kode verifikasi</Text> 
                    <TouchableOpacity onPress={()=>onRegenerateOTPPress()}>
                    <Text style={styles.footer2}>Kirim Ulang</Text>
                    </TouchableOpacity>
                </View>
               
            </View>
           
    
    
    )

}

export default OTPHandle