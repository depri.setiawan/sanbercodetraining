import React,{useEffect, useRef, useState} from 'react'
import {View,StyleSheet,Text,TextInput,TouchableOpacity,Image,Modal,Alert} from 'react-native'
import api from '../../api/index'
import {colors} from '../../style/colors'
import {Button} from '../../component/Button'
import Asyncstorage from '@react-native-async-storage/async-storage'
import { RNCamera } from 'react-native-camera'; 
import Icon from 'react-native-vector-icons/Feather';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios'
import { TextInputMask } from 'react-native-masked-text'


const Bantu=({navigation})=>{
    let input=useRef(null)
    let camera=useRef(null)
    let moneyField = useRef(null)

    const[token,setToken]=useState('')
    const[title,setTitle]=useState('')
    const[description,setDescription]=useState('')
    const[donation,setDonation]=useState('')
    const[isVisible,setIsVisible]=useState(false)
    const[type,setType]=useState('back')
    const[photo,setPhoto]=useState('')
    
    
    const takePicture=async()=>{
        const options={quality:0.5,based64:true}
        if(camera){
            const data=await camera.current.takePictureAsync(options)
            setPhoto(data);
            setIsVisible(false);
            console.log("tambahDonasi->data",data);
        }
    }

    useEffect(()=>{
        const getToken=async()=>{
            try{
                const token=await Asyncstorage.getItem('token')
                if(token!==null){
                    setToken(token)
                    console.log("buatDonasi->token",token)
                }
            }catch(err){
                console.log(err)
            }
        }
        getToken()
    },[])


    const onSavePress=()=>{
        const formData=new FormData()
        if(photo.uri==null){
        
            Alert.alert('Perhatian','Foto Donasi belum terisi');
                        
        }else{
            formData.append('title',title)
            formData.append('description',description)
            formData.append('donation',moneyField.current.getRawValue())
            formData.append('photo',{
                uri: Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
                name:'photo.jpg',
                type:'image/jpg'
            })
        
            
        axios.post(`${api.api}/donasi/tambah-donasi`,formData,{
            timeout:20000,
            headers:{
                'Authorization':'Bearer'+token,
                'Accept':'application/json',
                'Content-Type':'multipart/form-data'
            }
        })
        .then((res)=>{
            console.log("tambahDonasi-res",res)
            navigation.goBack()
            
        })
        .catch((error)=>{
            console.log("tambahDonasi-error",error)
        })
        console.log(photo.uri);
        console.log(formData)
        }
    }
  


    const renderCamera=()=>{
        return(
            <Modal visible={isVisible} onRequestClose={()=>setIsVisible(false)}>
                <View style={{flex:1,justifyContent:'center',alignContent:'center'}}>
                    <RNCamera
                        style={{flex:1,justifyContent:'flex-end',alignItems:'center'}}
                        type={type}
                        ref={camera}
                        >

                        <View style={styles.bntTakeContainer}>
                            <TouchableOpacity style={styles.btnTake} onPress={()=>takePicture()}>
                                <Icon name="camera" size={30}/>
                            </TouchableOpacity>
                        </View>

                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return(
        <View style={styles.container}>
            <View style={styles.containerPhoto}>
            {photo.uri!==null?(<Image source={{uri:photo.uri}} style={{width:'100%',height:'100%'}}/>):""}      
                <TouchableOpacity onPress={()=>setIsVisible(true)} style={styles.iconCamera}>
                    <Icon name="camera" size={20} color="#ffffff" />
                    <Text>Pilih Gambar</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.detailProfiles}>
                    <Text style={styles.editTitle}>Judul</Text>
                    <TextInput style={styles.editItem}
                            ref={input}
                            value={title}
                            style={styles.input}
                            onChangeText={(value)=>setTitle(value)}
                            underlineColorAndroid={'#aaaaaa'}
                        />
                <Text style={styles.editTitle}>Deskripsi</Text>
                <TextInput style={styles.editItem} 
                    value={description}
                    style={styles.input}
                    onChangeText={(value)=>setDescription(value)}
                    underlineColorAndroid={'#aaaaaa'}
                />
                 <Text style={styles.editTitle}>Dana Yang Dibutuhkan</Text>
                
                <TextInputMask
                    type={'money'}
                    style={styles.input}
                    options={{
                        precision: 0,
                        separator: ',',
                        delimiter: '.',
                        unit: 'RP. ',
                        suffixUnit: ''
                    }}
                    value={donation}
                    ref={moneyField}
                    placeholder="Rp. 0"
                    onChangeText={(value) => setDonation(value)}
                />


            <Button  onPress={() =>onSavePress()}>
                        <View style={styles.saveBtn}>
                            <Text style={styles.btnSaveText}>Simpan</Text>
                        </View>
                    </Button>
            </View>
            {renderCamera()}
            </View>
    )
}

export default Bantu;

const styles=StyleSheet.create({
    container:{
        flex:1,
        width:'100%'
    },
    containerPhoto:{
        marginBottom:50,
        height:200,
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#cccccc'
    },
    profilePhoto:{
        width:80,
        height:80,
        borderRadius:50    
    },
    iconCamera:{
        position:'absolute',
        justifyContent:'center',
        alignItems:'center',
        width:150,
        height:30,
        top:80,
        left:130,
        
    },  
    detailProfiles:{
        flex:1,
        padding:10,
        width:'100%'
    },
    editTitle:{
        color:'#555555',
        fontWeight:'bold'

    },
    nameContainer:{
        flexDirection:'row',
        width:'100%',
        alignItems:'center'
    } ,
    
    editItem:{
        color:colors.black
        
    },
        
   
    saveBtn:{
        height:50,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:colors.blue,
    },
    btnSaveText:{
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.white
    },
    camFlipContainer:{
        alignItems:'flex-start',
        width:400,
        padding:10
    },
    btnFlip:{
        backgroundColor:colors.white,
        height:50,
        width:50,
        borderRadius:50,
        alignItems:'center',
        justifyContent:'center'
   
    },
    round:{
        height:200,
        width:150,
        borderWidth:1,
        borderColor:colors.white,
        borderRadius:150
    },
    rectangle:{
        width:150,
        height:150,
        borderWidth:1,
        borderColor:colors.white,
        borderRadius:10
    },
    bntTakeContainer:{
        backgroundColor:colors.white,
        height:80,
        width:80,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:80,
        marginBottom:10
    }



   
});