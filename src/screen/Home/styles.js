import {StyleSheet} from 'react-native';
import {colors} from '../../style/colors'
import { Dimensions } from "react-native";
import { color } from 'react-native-reanimated';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container:{
        flex:1,
        width:width,
        height:height,
        backgroundColor:colors.white
    },
    childContainter1:{
        height:40,
        backgroundColor:colors.blue,
        alignItems:'center'
    },
    
    

    childBlueCard1:{
        backgroundColor:colors.blue,
        width:'25%',
        justifyContent:'center',
        padding:10,
        borderRadius:5
    },

    containerSaldoTitle:{
        flexDirection:'row',
    },  
    titleSaldo:{
        color:colors.white,
        marginLeft:5
    },
    
    textSaldo:{
        color:colors.white,
        textAlign:'left'
    },  

    childCard:{
        width:'25%',
        alignItems:'center',
       
    },  
    childContainer2:{
        height:'80%',
        backgroundColor:colors.white
        
    },
    card1:{
        backgroundColor:colors.white,
        width:'97%',
        height:80,
        borderRadius:20,
        padding:20,
        shadowColor: "#000",
        shadowOpacity: 1,
        shadowRadius: 1,
        elevation: 5,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center'

    },
    card2:{
        marginTop:50,
        backgroundColor:colors.white,
        marginBottom:20
    },
    containerChild2:{
        marginTop:10,
        flexDirection:'row'
    },
    card3:{
        borderTopColor:'#dddddd',
        borderTopWidth:10,
        padding:10
    },
    titleText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: colors.black
    },
    
    containerDonasi:{
        marginRight:10,
        borderRadius:20,
        borderColor:'#bbbbbb',
        borderWidth:1,
        overflow:"hidden",
        shadowOpacity: 1,
        shadowRadius: 1
    }
})

export default styles