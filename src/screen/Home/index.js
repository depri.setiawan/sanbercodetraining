import React,{useState,useEffect} from 'react'
import { View, Text, Image, StatusBar, FlatList, ImageBackground } from 'react-native'
import {colors} from '../../style/colors'
import styles from './styles'
import api from '../../api'
import Axios from 'axios'
import Asyncstorage from '@react-native-async-storage/async-storage'
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import wallet from '../../asset/images/wallet.png'
import { SliderBox } from "react-native-image-slider-box";
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler'


const Tab = createBottomTabNavigator();
const images=[
    require('../../asset/images/slider/child1.jpg'),
    require('../../asset/images/slider/child2.jpg'),
    require('../../asset/images/slider/child3.jpg'),
    require('../../asset/images/slider/child4.jpg'),
]

const Home=({navigation})=>{
    const [token, setToken] = useState('')
    const [danaMendesak, setDanaMendesak] = useState([
        {
            "id":1,
            "title":"Lorem opsum dolor siamet",
            "photo":require('../../asset/images/slider/child3.jpg'),
            "value":"100.000"
        }, 
        {
            "id":2,
            "title":"Lorem opsum dolor siamet",
            "photo":require('../../asset/images/slider/child4.jpg'),
            "value":"200.000"
        },
         {
            "id":3,
            "title":"Lorem opsum dolor siamet",
            "photo":require('../../asset/images/slider/child2.jpg'),
            "value":"300.000"
        },
        {
            "id":4,
            "title":"Lorem opsum dolor siamet",
            "photo":require('../../asset/images/slider/child1.jpg'),
            "value":"400.000"
        },
      ]);
    

    useEffect(() => {
        const getToken=async()=>{
            try{
                const token=await Asyncstorage.getItem('token')
                if(token!==null){
                    setToken(token)
                    

                }
            }catch(err){
                console.log(err)
            }
        }

        getToken();
       
    },[])

    const renderItem=({item,index})=>{
        return(
                    <View style={styles.containerDonasi}>
                     <Image source={item.photo} 
                        style={{width:200,
                                height:150,
                                
                                }}/>
                         <View style={{padding:10}}>
                            <Text >{item.title}</Text>    
                            <Text >Rp. {item.value}</Text>    
                         </View>
                        
                     </View>
                 
        );
      }

    return(
        <View style={styles.container}>
            <StatusBar backgroundColor={colors.blue} barStyle="light-content" />
            <ScrollView>
            <View style={styles.childContainter1}>
            
                <View style={styles.card1}>
                    <View style={styles.childBlueCard1}>
                        <View style={styles.containerSaldoTitle}>
                        <Image source={wallet} style={{height:20,width:20}}/> 
                        <Text style={styles.titleSaldo}>Saldo</Text>
                        </View>          
                        <Text style={styles.textSaldo}>Rp. 0</Text>
                    </View>
                    <View style={styles.childCard}>
                    <MaterialCommunityIcons name="wallet-plus-outline" size={30} color="#000000" />
                    <Text>Isi</Text>
                       
                    </View>

                    <View style={styles.childCard}>
                        <MaterialCommunityIcons name="autorenew" size={30} color="#000000" />
                        <Text>Riwayat</Text>
                       
                    </View>

                    <View style={styles.childCard}>
                     <MaterialCommunityIcons name="apps" size={30} color="#000000" />
                     <Text>Lainnya</Text>
                       
                    </View>
                    
                </View>
                
            </View>
         
            <View style={styles.childContainter2}>
                <View style={styles.card2}>
                <SliderBox images={images} 
                ImageComponentStyle={{borderRadius: 15, width: '97%', marginTop: 5}}
                autoplay={true}
                circleLoop/>
                <View style={styles.containerChild2}>
                    <View style={styles.childCard}>
                        <TouchableOpacity onPress={()=>navigation.navigate('Donasi')}>
                        <Image source={require('../../asset/images/donation.png')} style={{width:50,height:50}}/>
                        <Text style={{textAlign:'center'}}>Donasi</Text>
                        </TouchableOpacity>  
                    </View>  
                    <View style={styles.childCard}>
                        <TouchableOpacity onPress={()=>navigation.navigate('Chart')}>
                       
                        <Image source={require('../../asset/images/analytics.png')} style={{width:50,height:50}}/>
                        <Text style={{textAlign:'center'}}>Statistik</Text>  
                        </TouchableOpacity>
                    </View> 
                    <View style={styles.childCard}>
                        <TouchableOpacity onPress={()=>navigation.navigate('History')}>
                        <Image source={require('../../asset/images/clock.png')} style={{width:50,height:50}}/>
                        <Text style={{textAlign:'center'}}>Riwayat</Text>  
                        </TouchableOpacity>
                    </View>
                     <View style={styles.childCard}>
                        <TouchableOpacity onPress={()=>navigation.navigate('Bantu')}>
                        <Image source={require('../../asset/images/savings.png')} style={{width:50,height:50}}/>
                        <Text style={{textAlign:'center'}}>Bantu</Text>  
                        </TouchableOpacity>
                    </View>     
                </View>

            </View>
                <View style={styles.card3}>
                    <Text style={styles.titleText}>Penggalangan Dana Mendesak</Text>
                    <View style={{marginTop:15}}>
                        <FlatList
                        horizontal
                        data={danaMendesak}
                        renderItem={renderItem}
                        keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </View>
            </View>

            </ScrollView>
                
        </View>
       
    )
}

export default Home