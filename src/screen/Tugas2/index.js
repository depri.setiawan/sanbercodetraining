/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import React,{useEffect,useState} from 'react';
import {StatusBar, Text, View, StyleSheet, Image,TouchableOpacity,Alert,TextInput} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import api from '../../api'
import Axios from 'axios'
import Asyncstorage from '@react-native-async-storage/async-storage'
import { GoogleSignin } from '@react-native-community/google-signin';


function Tugas2({navigation}) {

  const [profiles,setProfile]=useState({})
  const [userInfo,setUserInfo]=useState(null)
  const [isGoogle,setIsGoogle]=useState(false)

  useEffect(()=>{
      async function getToken(){
        try{
          const token=await Asyncstorage.getItem("token")
          console.log('GetToken->token',token)
          getProfile(token)
          console.log('GetProfile->profile',profiles)
        }catch(err){
          console.log(err)
        }
      }
      getToken()
      getCurrentUser();
      getProfile();
      console.log("UserInfo",userInfo);
  },[])

  const getCurrentUser=async()=>{
    try{
      const userInfo=await GoogleSignin.signInSilently();
      console.log("getCurrentUser->userInfo",userInfo);
      if(userInfo){
        setUserInfo(userInfo)
        setIsGoogle(true)
      }
    }catch(e){
      console.log(e)
    }
    
  }

  const getProfile=(token)=>{
    Axios.get(`${api.api}/profile/get-profile`,{
      timeout:20000,
      headers:{
        'Authorization' : 'Bearer'+token,
        'Accept':'application/json',
        'Context-Type':'application/json'
      }
    })
    .then((res)=>{
      const data=res.data.data.profile;
      setProfile(data)
     
    })
    .catch((e)=>{
      console.log(e)
        
    })
  }

  const onLogoutPress=()=>{
    try{
      Alert.alert('Keluar','Apakah anda yakin untuk keluar???',
      [
          {text:'No'
          },
          {
           text:'Yes',
           onPress:async()=>{
            if(isGoogle){
              await GoogleSignin.revokeAccess()
              await GoogleSignin.signOut()
            }
            await Asyncstorage.removeItem("token")
            navigation.reset({
              index: 0,
              routes: [{ name: 'Login' }],
           });
           }

          }
      ]);
     
    }catch(e){
      console.log(e)
    }
  }

  const editProfile=()=>{
    navigation.navigate('EditAccount',{
      name:profiles.name,
      email:profiles.email,
      photo:profiles.photo
    })
  }

  const showMaps=()=>{
    navigation.navigate('Map')
  }


  function Header() {
    return (
      <View style={styles.header}>
        <Text style={styles.textHeader}>Account</Text>
      </View>
    );
  }
  
  function Profile(props) {
    return (
      <View style={styles.profile}>
        <View style={{width: '20%'}}>
          <Image
            style={styles.phofoProfile}
            source={isGoogle==false?props.photo==null?require('../../asset/images/avatar.png'):{uri:api.home+props.photo+'?'+new Date(),cache:'reload',headers:{Pragma:'no-cache'}}:{uri:props.photo}}
          /> 

        </View>
        <View style={{width: '80%'}}>
          <Text style={{fontWeight: 'bold', fontSize: 14}}>
            {props.name}</Text>
        </View>
      </View>
    );
  }
  
  function Menu() {
    return (
      <View style={styles.menu}>
        <View style={styles.submenu1}>
          <View style={{width: '10%'}}>
            <Icon name="ios-wallet-outline" size={30} color="#000000" />
          </View>
          <View style={{width: '30%'}}>
            <Text style={{textAlign: 'left'}}>Saldo</Text>
          </View>
          <View style={{width: '60%'}}>
            <Text style={{textAlign: 'right'}}>Rp. 120.000.000</Text>
          </View>
        </View>
        <View style={styles.submenu2}>
          <View style={{width: '10%'}}>
            <Icon name="settings-outline" size={30} color="#000000" />
          </View>
          <View style={{width: '90%'}}>
            <Text>Pengaturan</Text>
          </View>
        </View>
        <View style={styles.submenu2}>
          <View style={{width: '10%'}}>
            <Icon name="help-circle-outline" size={30} color="#000000" />
          </View>
          <View style={{width: '90%'}}>
            <TouchableOpacity onPress={()=>showMaps()}>
              <Text>Bantuan</Text>
              </TouchableOpacity>
          </View>
        </View>
        <View style={styles.submenu1}>
          <View style={{width: '10%'}}>
            <Icon name="newspaper-outline" size={30} color="#000000" />
          </View>
          <View style={{width: '90%'}}>
            <Text>Syarat &amp; Ketentuan </Text>
          </View>
        </View>
        <View style={styles.submenu2}>
          <View style={{width: '10%'}}>
            <Icon name="ios-exit-outline" size={30} color="#000000" />
          </View>
          <TouchableOpacity onPress={()=>onLogoutPress()}>
          <View style={{width: '100%'}}>
            <Text>Keluar</Text>
          </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }



  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" backgroundColor="#3a86ff" />
      {/* <Header /> */}
      {/*<Profile name={profiles.name} photo={profiles.photo}/>*/}
      <TouchableOpacity onPress={()=>editProfile()}>
      <Profile 
      name={isGoogle==false?profiles.name==null?'Depri Setiawan':profiles.name:userInfo && userInfo.user && userInfo.user.name} 
      photo={isGoogle==false?profiles.photo==null?null:profiles.photo:userInfo && userInfo.user && userInfo.user.photo}/>
      </TouchableOpacity>
      <Menu />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
  },
  header: {
    flexDirection: 'row',
    backgroundColor: '#3a86ff',
    height: 60,
    padding: 10,
    alignItems: 'center',
  },
  textHeader: {
    textAlign: 'right',
    color: '#ffffff',
    fontSize: 18,
    fontWeight: 'bold',
  },
  profile: {
    flexDirection: 'row',
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
    height: 80,
    padding: 15,
    borderBottomColor: '#f2f2f2',
    borderBottomWidth: 2,
  },
  phofoProfile: {
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  menu: {
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },

  submenu1: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 60,
    width: '100%',
    padding: 15,
    borderBottomColor: '#f2f2f2',
    borderBottomWidth: 5,
  },
  submenu2: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 60,
    width: '100%',
    padding: 15,
    borderBottomColor: '#f2f2f2',
    borderBottomWidth: 1,
  },

  item: {
    flexDirection: 'row',
    width: '50%',
  },
});

export default Tugas2;
