import React,{useEffect, useRef, useState} from 'react'
import {View,StyleSheet,Text,TextInput,TouchableOpacity,Image,Modal} from 'react-native'
import api from '../../api/index'
import {colors} from '../../style/colors'
import {Button} from '../../component/Button'
import Asyncstorage from '@react-native-async-storage/async-storage'
import { RNCamera } from 'react-native-camera'; 
import Icon from 'react-native-vector-icons/Feather';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios'
import avatar from '../../asset/images/avatar.png'
import { NavigationHelpersContext } from '@react-navigation/native'




const EditAccount=({navigation,route})=>{
    let input=useRef(null)
    let camera=useRef(null)
    const[editable,setEditable]=useState(false)
    const[token,setToken]=useState('')
    const[name,setName]=useState(route.params.name)
    const[email,setEmail]=useState(route.params.email)
    const[isVisible,setIsVisible]=useState(false)
    const[type,setType]=useState('back')
    const[photo,setPhoto]=useState('')
    const[avatars,setAvatars]=useState(route.params.photo)

    const toggleCamera=()=>{
        setType(type=='back'?'front':'back')
    }


    const takePicture=async()=>{
        const options={quality:0.5,based64:true}
        if(camera){
            const data=await camera.current.takePictureAsync(options)
            setPhoto(data);
            setIsVisible(false);
            console.log("editAccount->data",data);
        }
    }

    useEffect(()=>{
        const getToken=async()=>{
            try{
                const token=await Asyncstorage.getItem('token')
                if(token!==null){
                    setToken(token)
                }
            }catch(err){
                console.log(err)
            }
        }
        getToken()
    },[])



    const editData=()=>{
        setEditable(!editable)
    }

    const onSavePress=()=>{
        const formData=new FormData()
        if(photo.uri!=null){
        formData.append('name',name)
        formData.append('photo',{
            uri: Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
            name:'146.jpg',
            type:'image/jpg'
        })}else{
            formData.append('name',name)
       
        }
            
        axios.post(`${api.api}/profile/update-profile`,formData,{
            timeout:20000,
            headers:{
                'Authorization':'Bearer'+token,
                'Accept':'application/json',
                'Content-Type':'multipart/form-data'
            }
        })
        .then((res)=>{
            console.log("editAccount-res",res)
            navigation.goBack()
            
        })
        .catch((error)=>{
            console.log("editAccount-error",error)
        })
        console.log(photo.uri);
    }

    const renderCamera=()=>{
        return(
            <Modal visible={isVisible} onRequestClose={()=>setIsVisible(false)}>
                <View style={{flex:1,justifyContent:'center',alignContent:'center'}}>
                    <RNCamera
                        style={{flex:1,justifyContent:'space-between',alignItems:'center'}}
                        type={type}
                        ref={camera}
                        >

                        <View style={styles.camFlipContainer}>
                            <TouchableOpacity style={styles.btnFlip} onPress={()=>toggleCamera()}>
                                <Icon name="repeat" size={30}/>
                            </TouchableOpacity>

                        </View>
                        <View style={styles.round}/>
                        <View style={styles.rectangle}/>
                        <View style={styles.bntTakeContainer}>
                            <TouchableOpacity style={styles.btnTake} onPress={()=>takePicture()}>
                                <Icon name="camera" size={30}/>
                            </TouchableOpacity>
                        </View>

                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return(
        <View style={styles.container}>
            <View style={styles.profiles}>
                <Image source={photo.uri==null?avatars==null?require('../../asset/images/avatar.png'):{uri:api.home+avatars+'?'+new Date(),cache:'reload',headers:{Pragma:'no-cache'}}:{uri:photo.uri}} style={styles.profilePhoto}/>
                <TouchableOpacity onPress={()=>setIsVisible(true)} style={styles.profileCamera}>
                    <Icon name="camera" size={20} color="#ffffff" />
                </TouchableOpacity>
            </View>

            <View style={styles.detailProfiles}>
                    <Text style={styles.editTitle}>Nama Lengkap</Text>

                    <View style={{flexDirection:'row'}}>
                    <View style={{width:'90%'}}>
                        <TextInput style={styles.editItem}
                            ref={input}
                            value={name}
                            editable={editable}
                            style={styles.input}
                            onChangeText={(value)=>setName(value)}
                            underlineColorAndroid={'#aaaaaa'}
                        />
                    </View>
                    <TouchableOpacity onPress={()=>editData()}>
                    <Icon name="edit-2" size={20} color="#000000" />
                    </TouchableOpacity>
                    </View>
                <Text style={styles.editTitle}>Email</Text>
                <TextInput style={styles.editItem} 
                    value={email}
                    editable={false}
                    style={styles.input}
                    onChangeText={(value)=>setEmail(value)}
                    underlineColorAndroid={'#aaaaaa'}
                />


            <Button  onPress={() =>onSavePress()}>
                        <View style={styles.saveBtn}>
                            <Text style={styles.btnSaveText}>Simpan</Text>
                        </View>
                    </Button>
            </View>
            {renderCamera()}
            </View>
    )
}

export default EditAccount;

const styles=StyleSheet.create({
    container:{
        flex:1,
        width:'100%'
    },
    profiles:{
        marginBottom:50,
        marginTop:20,
        height:100,
        justifyContent:'center',
        alignItems:'center'
    },
    profilePhoto:{
        width:80,
        height:80,
        borderRadius:50    
    },
    profileCamera:{
        position:'absolute',
        justifyContent:'center',
        alignItems:'center',
        width:30,
        height:30,
        top:60,
        right:160,
        backgroundColor:'#999999',
        borderRadius:10
        
    },  
    detailProfiles:{
        flex:1,
        padding:10,
        width:'100%'
    },
    editTitle:{
        color:'#555555',
        fontWeight:'bold'

    },
    nameContainer:{
        flexDirection:'row',
        width:'100%',
        alignItems:'center'
    } ,
    
    editItem:{
        color:colors.black
        
    },
        
   
    saveBtn:{
        height:50,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:colors.blue,
    },
    btnSaveText:{
        fontSize: 14,
        fontWeight: 'bold',
        color: colors.white
    },
    camFlipContainer:{
        alignItems:'flex-start',
        width:400,
        padding:10
    },
    btnFlip:{
        backgroundColor:colors.white,
        height:50,
        width:50,
        borderRadius:50,
        alignItems:'center',
        justifyContent:'center'
   
    },
    round:{
        height:200,
        width:150,
        borderWidth:1,
        borderColor:colors.white,
        borderRadius:150
    },
    rectangle:{
        width:150,
        height:150,
        borderWidth:1,
        borderColor:colors.white,
        borderRadius:10
    },
    bntTakeContainer:{
        backgroundColor:colors.white,
        height:80,
        width:80,
        alignItems:'center',
        justifyContent:'center',
        borderRadius:80,
        marginBottom:10
    }



   
});