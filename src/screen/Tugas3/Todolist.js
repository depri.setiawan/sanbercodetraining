import React, {useState} from 'react';
import {Button,StatusBar,StyleSheet,Text,TextInput,TouchableOpacity,View,FlatList} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const Tugas3 = () => {

  const [inputData, setInputData] = useState([]);
  const [text,setText]=useState('');
  const [index,setIndex]=useState(0);


  const getCurrentDate=()=>{
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    month=(month<10 ?'0' + month:month); 
    var year = new Date().getFullYear();
    return date + '-' + month + '-' + year;//format: dd-mm-yyyy;
  }


  const addData = (text) => {  
    if(text!=""){
      setInputData([
        ...inputData,
        {
          index:index,
          tgl:getCurrentDate(),
          name: text
        }
      ]);
      setText('');
      setIndex(index+1);
    }
    
    console.log('data',inputData);      
  };

  const hapusData=(index)=>{
    setInputData(inputData.filter((item) => item.index !== index));
  }

   return (
    <View style={styles.wrapper}>
      <StatusBar barStyle="dark-content" backgroundColor="#ffffff" />
      <Text>Masukkan Todolist</Text>
      <View style={styles.inputWrapper}>
        <TextInput style={styles.textInput}
          onChangeText={(text) => setText(text)}
          value={text}
        />
        <TouchableOpacity activeOpacity = { 0.8 } style={styles.button} onPress={()=>addData(text)}>
            <Text style={{color:'#ffffff',fontWeight:'bold'}}>+</Text>
        </TouchableOpacity>
       
        
      </View>
      <View>
      {inputData.map((element) => {
      return (
          <View key={element.index} style={styles.item}>
            <View>
            <Text>{element.tgl}</Text>
            <Text>{element.name}</Text>
            </View>
            <TouchableOpacity onPress={()=>hapusData(element.index)}>
            <Icon name="trash-outline" size={30} color="#000000" />
            </TouchableOpacity>
          </View>
        
        );
      })}
       
      </View>
     
    </View>
  );
};

export default Tugas3;

const styles=StyleSheet.create({
   wrapper:{
      padding:15
   },
   inputWrapper:{
     flexDirection:'row',
     height:50,
     marginBottom:10
     
   },
   textInput:{
     width:'85%',
     borderColor:'gray',
     borderWidth:1
   },
   button: {
    alignItems: "center",
    padding: 10,
    width:'15%',
    justifyContent:'center',
    backgroundColor:'#3498db'
  },
  item:{
    flexDirection:'row',
    padding:20,
    borderWidth:1,
    borderColor:'grey',
    marginBottom:10,
    alignItems:'center',
    justifyContent:'space-between'
  }

});