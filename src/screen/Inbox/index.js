import React, { useEffect,useState } from 'react'
import {View,Text,StyleSheet, SnapshotViewIOS} from 'react-native'
import {colors} from '../../style/colors'
import api from '../../api'
import Axios from 'axios'
import Asyncstorage from '@react-native-async-storage/async-storage'
import database from '@react-native-firebase/database';
import { GiftedChat } from 'react-native-gifted-chat'

const Inbox=()=>{
    const[user,setUser]=useState({})
    const[messages,setMessages]=useState([])

    useEffect(()=>{
        const getToken=async()=>{
            try{
                const token=await Asyncstorage.getItem("token")
                console.log('GetToken->token',token)
                if(token!==null){
                    return getProfile(token);
                }    
            }catch(err){
                console.log(err)
            }
        }
        getToken();
        getProfile();
        onRef();
        return()=>{
            const db=database().ref('messages')
            if(db){
                db.off()
            }
        }
    },[])

    const getProfile=(token)=>{
        Axios.get(`${api.api}/profile/get-profile`,{
            headers:{
                'Authorization':'Bearer'+token,
                'Accept':'application/json',
                'Context-Type':'application/json'      
            }
        }).then((res)=>{
            console.log("Account->res",res)
            const data=res.data.data.profile
            setUser(data)
        }).catch((err)=>{
            console.log("Account-err",err)
        })
    }
    const onRef=()=>{
        database().ref('messages').limitToLast(20).on('child_added',snapshot=>{
            const value=snapshot.val()
            setMessages(previouMessages=>GiftedChat.append(previouMessages,value))
        })
    }
    const onSend=((messages=[])=>{
            for(let i = 0 ;i <messages.length;i++){
                database().ref('messages').push({
                    _id:messages[i]._id,
                    createAt:database.ServerValue.TIMESTAMP,
                    text:messages[i].text,
                    user:messages[i].user
                })
            }
    })

    

    return(
        <View style={styles.container}>
            <GiftedChat
                messages={messages}
                onSend={(messages)=>onSend(messages)}
                user={{
                    _id:user.id,
                    name:user.name,
                    avatar:`${api.home}${user.photo}`
                }}
                showUserAvatar
            />
        </View>

    )
}

export default Inbox

const styles=StyleSheet.create({
    container:{
        flex:1
    }
})