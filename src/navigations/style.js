import React from 'react'
import { StyleSheet } from 'react-native'
import {colors} from '../style/colors'

const styles = StyleSheet.create({
    logo: {
        width: 30,
        height: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        margin: 10,
        marginRight: 10
    },
    searchText:{
        borderColor:colors.white,
        borderWidth:1,
        height:40,
        borderRadius:20,
        marginLeft:10,
        marginRight:10,
        color:'#ffffff',
        padding:10
        
    },
})

export default styles