import React, { useState, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Intro from '../screen/Intro/Intro';
import Splashscreen from '../screen/SplashScreen/Splashscreen';
import Login from '../screen/Login';
import Account from '../screen/Account';
import EditAccount from '../screen/Account/editAccount';
import Register from '../screen/Register';
import OTPHandle from '../screen/Otp/';
import SetupPassword from '../screen/Password'
import Dashboard from './Tab'
import Donasi from '../screen/Donasi'
import Map from '../screen/Map'
import Chart from '../screen/Chart'
import History from '../screen/History'
import Bantu from '../screen/Bantu'

import {colors} from '../style/colors';
import AsyncStorage from '@react-native-async-storage/async-storage'

const Stack = createStackNavigator()

const MainNavigation = (props) => (
    <Stack.Navigator
        initialRouteName={props.token !== null ? "Dashboard" : "Intro"}
    >
        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
        <Stack.Screen 
            name ="Login"
            component={Login}
            options={{
                title:'Masuk',
                headerStyle:{
                    backgroundColor:colors.blue
                },
                headerTintColor:colors.white
            }}
        />
        
        <Stack.Screen 
            name ="Account"
            component={Account}
            options={{
                title:'Account',
                headerStyle:{
                    backgroundColor:colors.blue
                },
                headerTintColor:colors.white
            }}
        />
        <Stack.Screen 
            name ="EditAccount"
            component={EditAccount}
            options={{
                title:'Edit Account',
                headerStyle:{
                    backgroundColor:colors.blue
                },
                headerTintColor:colors.white
            }}
        />
        <Stack.Screen 
            name ="Daftar"
            component={Register}
            options={{
                title:'Daftar',
                headerStyle:{
                    backgroundColor:colors.blue
                },
                headerTintColor:colors.white
            }}
        />
        <Stack.Screen 
            name ="OTPHandle"
            component={OTPHandle}
            options={{
                title:'Daftar',
                headerStyle:{
                    backgroundColor:colors.blue
                },
                headerTintColor:colors.white
            }}
        /> 
        <Stack.Screen 
            name ="SetupPassword"
            component={SetupPassword}
            options={{
                title:'Setup Password',
                headerStyle:{
                    backgroundColor:colors.blue
                },
                headerTintColor:colors.white
            }}
        />
        <Stack.Screen name="Dashboard" component={Dashboard} options={{ headerShown: false }} />
        <Stack.Screen 
            name ="Donasi"
            component={Donasi}
            options={{
                title:'Donasi',
                headerStyle:{
                    backgroundColor:colors.blue
                },
                headerTintColor:colors.white
            }}
        /> 
        
        <Stack.Screen 
            name ="Map"
            component={Map}
            options={{
                title:'Maps',
                headerStyle:{
                    backgroundColor:colors.blue
                },
                headerTintColor:colors.white
            }}
        /> 
        
        <Stack.Screen 
            name ="Register"
            component={Register}
            options={{
                title:'DAFTAR',
                headerStyle:{
                    backgroundColor:colors.blue
                },
                headerTintColor:colors.white
            }}
        /> 
        
        <Stack.Screen 
            name ="Chart"
            component={Chart}
            options={{
                title:'Statistik',
                headerStyle:{
                    backgroundColor:colors.blue
                },
                headerTintColor:colors.white
            }}
        />
        
        <Stack.Screen 
            name ="History"
            component={History}
            options={{
                title:'Riwayat',
                headerStyle:{
                    backgroundColor:colors.blue
                },
                headerTintColor:colors.white
            }}
        /> 
        
        <Stack.Screen 
            name ="Bantu"
            component={Bantu}
            options={{
                title:'Buat Donasi',
                headerStyle:{
                    backgroundColor:colors.blue
                },
                headerTintColor:colors.white
            }}
        /> 
    </Stack.Navigator>
)

const AppNavigation = () => {

    const [isLoading, setIsLoading] = useState(true)
    const [token, setToken] = useState(null)


    useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)

        async function getToken() {
            const token = await AsyncStorage.getItem("token")
            setToken(token)
        }

        getToken()

    },[])

    if(isLoading) {
        return <Splashscreen/>
    }

    return (
        <NavigationContainer>
            <MainNavigation token={token}/>
        </NavigationContainer>
    )
}

export default AppNavigation