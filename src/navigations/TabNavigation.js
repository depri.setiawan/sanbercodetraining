import React from 'react';
import {Image,TextInput} from 'react-native'
import Icon from 'react-native-vector-icons/Feather';
import { createStackNavigator } from '@react-navigation/stack';
import {colors} from '../style/colors';
import Home from '../screen/Home';
import Account from '../screen/Account';
import Inbox from '../screen/Inbox';
import styles from './style'
import Headers from '../component/Header'

const Stack = createStackNavigator()
export const HomeStack=()=>{
    return(
        <Stack.Navigator
        headerMode="screen">
            <Stack.Screen
                name="Home"
                component={Home}
                options={{
                    headerLeft: props => <Image source={require('../asset/images/heart.png')} style={styles.logo} />,
                    headerLeftContainerStyle: {
                        margin: 10,
                        alignItems: 'center',
                        justifyContent: 'flex-start'
                    },
                    headerTitle: props => (
                        <TextInput
                        placeholder='Cari Disini'
                        placeholderTextColor="#ffffff"
                        style={styles.searchText} 
                    />  
                    ),
                    headerRight: props => <Icon name="heart" size={25} color={colors.white} style={styles.icon} />,
                    headerStyle: {
                        elevation: 0,
                        shadowOpacity: 0,
                        backgroundColor: colors.blue
                    },
                    headerTintColor: colors.white
                }}
            />

            
        </Stack.Navigator>
    )
}

export const InboxStack = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
                name="Inbox"
                component={Inbox}
                options={{
                    title: 'Inbox',
                    headerStyle: {
                        backgroundColor: colors.blue
                    },
                    headerTintColor: colors.white
                }}
            />
        </Stack.Navigator>
    )
}

export const AccountStack=()=>{
    return(
        <Stack.Navigator>
            <Stack.Screen
            name="Account"
            component={Account}
            options={{
                title:'Account',
                headerStyle:{
                    backgroundColor:colors.blue
                },
                headerTintColor:colors.white
            }}
            />

            
        </Stack.Navigator>
    )
}



