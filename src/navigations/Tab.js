import React from 'react'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import { HomeStack,InboxStack, AccountStack } from './TabNavigation';
import {colors} from '../style/colors';
import Icon from 'react-native-vector-icons/Feather';

const Tab = createMaterialBottomTabNavigator()

const Dashboard = () => {
    return (
        <Tab.Navigator
            shifting={true}
            initialRouteName="HomeStack"
            activeColor={colors.blue}
            barStyle={{ backgroundColor: colors.white }}
        >
            <Tab.Screen
                name="HomeStack"
                component={HomeStack}
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color, size }) => <Icon name="home" size={23} color={color} />
                }}
            />
            <Tab.Screen
                name="InboxStack"
                component={InboxStack}
                options={{
                    tabBarLabel: 'Inbox',
                    tabBarBadge: 0,
                    tabBarIcon: ({ color, size }) => <Icon name="message-square" size={23} color={color} />
                }}
            />
            <Tab.Screen
                name="AccountStack"
                component={AccountStack}
                options={{
                    tabBarLabel: 'Account',
                    tabBarIcon: ({ color, size }) => <Icon name="user" size={23} color={color} />
                }}
            />
        </Tab.Navigator>
    )
}

export default Dashboard