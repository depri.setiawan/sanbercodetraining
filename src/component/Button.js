import React from 'react';
import {View,TouchableOpacity,StyleSheet} from 'react-native';

export const Button=(props)=>{
    return(
    <TouchableOpacity style={[{...props.styles},styles.btn]} onPress={props.onPress} activeOpacity={0.7}>
         {props.children}   
    </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    btn: {
        marginTop: 5,
        marginBottom: 5,
        marginRight:10
    }
})

